<?php

namespace Itbid\DataTable\Service;

use Itbid\DataTable\DataTable\DataTableInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\RouterInterface;
use Itbid\DataTable\DataType\StringDataType;

class DataTableGenerator
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var RouterInterface
     */
    private $router;

    private $columns = [];

    private $rows = [];

    private $options = [];

    /**
     * @var ContainerInterface $container
     */
    private $container;

    /**
     * @var DataTableInterface
     */
    private $dataTable;

    private $request;

    public function __construct(ContainerInterface $container, RouterInterface $router, RequestStack $request)
    {
        $this->em = $container->get('doctrine.orm.entity_manager');
        $this->router = $router;
        $this->request = $request;
        $this->container = $container;
    }

    public function configureDataTable(string $dataTable, array $options = []): self
    {
        $this->dataTable = new $dataTable($this->em, $this->router, $this->request, $this->container);
        $this->options = $this->resolveOptions($this->dataTable, $options);

        $this->dataTable->configureFields($this->options);
        $this->dataTable->configureActions($this->options);
        $this->dataTable->configurePagination($this->options);
        $this->dataTable->configureFilters($this->options);

        return $this;
    }

    private function getFilters()
    {
        //HACER getFilters()
    }

    private function getColumns()
    {
        if (!empty($this->columns)) {
            return $this->columns;
        }

        foreach ($this->dataTable->getFields() as $field) {
            $this->columns[] = $this->configureColumn($field);
        }

        if (count($this->dataTable->getActions())) {
            $this->columns[] = $this->configureColumn([
                'field' => 'actions',
                'title' => 'Acciones',
                'isAction' => true
            ]);
        }

        return $this->columns;
    }

    private function configureColumn($field): array
    {
        return [
            'label' => $field['title'],
            'field' => $field['field'],
            'options' => $field['options'],
            'dataType' => $field['options']['dataType']?new $field['options']['dataType']($this->container,$field['options']):[],
            'normalizedField' => str_replace('.', '_', $field['field']),
            'sort' => true,
            'isAction' => array_key_exists('isAction', $field) ? $field['isAction'] : false
        ];
    }

    private function getRows()
    {
        if (!empty($this->rows)) {
            return $this->rows;
        }

        $pa = PropertyAccess::createPropertyAccessor();
        foreach ($this->dataTable->buildQuery($this->options) as $row) {
            $finalRow = [];

            foreach ($this->getColumns() as $column) {
                if ($column['isAction']) {
                    continue;
                }

                $prefix = '';
                $postfix = '';
                if (is_array($row)) {
                    $prefix = '[';
                    $postfix = ']';
                }
                $properties = explode('.', $column['field']);

                $tmpValue = $row;
                foreach ($properties as $prop) {
                    if (null === $tmpValue) {
                        continue;
                    }
                    $tmpValue = $pa->getValue($tmpValue, $prefix.$prop.$postfix);
                }

                // Obtenemos el méttodo a ejecutar si es excel o no
                $method = 'transform';
                if ($this->dataTable->renderExcel()) {
                    $method = 'toString';
                }

                // Obtenemos el valor que debemos procesar
                if ($tmpValue !== $row) {
                    $toProccessValue = $tmpValue;
                } else {
                    $toProccessValue = $pa->getValue($row, $prefix.$column['field'].$postfix);
                }

                // Procesamos el valor en función de si el type tiene transformer definido o no
                if ($column['options']['transformer'] !== null) {
                    $toProccessValue = $column['options']['transformer']($toProccessValue);
                }

                // finalizamos la transformación de los valores con el método del dataType
                $finalRow[$column['normalizedField']] = $column['dataType']->$method($toProccessValue);
            }

            $finalRow['actions'] = $this->dataTable->getListActions();

            $this->rows[] = $finalRow;
        }

        return $this->rows;
    }

    public function getResults(): array
    {
        return [
            'columns' => $this->getColumns(),
            'rows' => $this->getRows(),
            'actions' => $this->dataTable->getPrimaryActions(),
            'pagination' => $this->dataTable->getPagination(),
            'filters' => $this->dataTable->getFilters(),
        ];
    }

    private function getDefaultOptionResolver()
    {
        $resolver = new OptionsResolver();

        $resolver->setRequired(['data_class']);
        $resolver->setAllowedTypes('data_class', ['string']);

        return $resolver;
    }

    /**
     * @param DataTaleInterface $dataTable
     * @param array $options
     * @return array
     */
    private function resolveOptions(DataTableInterface $dataTable, array $options): array
    {
        $resolver = $this->getDefaultOptionResolver();
        $dataTable->configureOptions($resolver);
        return $resolver->resolve($options);
    }
}