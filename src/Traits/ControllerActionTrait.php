<?php

namespace Itbid\DataTable\Traits;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait ControllerActionTrait
{

    public function renderDatatable(string $template, string $dt, array $options = []): Response
    {
        $request = Request::createFromGlobals();
        $generator = $this->container->get('datatable.generator');
        $result = $generator->configureDataTable($dt, $options)->getResults();
        $this->addData('list', $result);
        if ($request->get('render_excel')) {
            return $this->renderExcelDatatable($result);
        }

        if ($request->isMethod('post')) {
            return new JsonResponse(['list' => $result]);
        }
        return $this->render($template, $this->getData());
    }

    /**
     * Esta función crea una dependencia transversal entre itbid y datatable
     * Ahora por las prisas lo vamos a mantener pero luego habrá que desacoplar esta funcionalidad de itbid
     * @param array $result
     * @return mixed
     */
    protected function renderExcelDatatable(array $result)
    {
        $excelExport = $this->get('itbid_informes.itbid_excel');
        $header = [];
        foreach ($result['columns'] as $column) {
            $header[] = $column['label'];
        }

        return $excelExport->getExcel($header, $result['rows'], $this->get('translator'), false);
    }
}