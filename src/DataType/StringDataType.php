<?php

namespace Itbid\DataTable\DataType;

use Symfony\Component\OptionsResolver\OptionsResolver;


class StringDataType extends AbstractDataType
{
    public function transform($data): string
    {
        if ($this->options['translatable'] === true) {
            return $this->trans->trans((string) $data);
        }

        return (string) $data;
    }

    public static function configureOptions(OptionsResolver $options)
    {
        $options
            ->setDefault('translatable', false)
            ->setAllowedTypes('translatable', 'bool');
    }
}