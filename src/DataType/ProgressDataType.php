<?php

namespace Itbid\DataTable\DataType;

use App\Itbid\AdministracionBundle\Controller\ItbidBaseController;
use Exception;

class ProgressDataType extends AbstractDataType
{

    /**
     * @throws Exception
     */
    public function transform($data): string
    {
        $data = (float) $data;
        $num_decimal = $_SESSION['_sf2_attributes'][ItbidBaseController::ITBID_NUMBER_DECIMAL];
        $percentage = number_format($data,$num_decimal);

        return '<progress max="100" value="'.$percentage.'">'.$percentage. '%</progress> '.$percentage.'%';
    }

    public function toString($data): string
    {
        return number_format($data, 2).'%';
    }

}