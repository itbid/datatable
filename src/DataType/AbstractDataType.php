<?php

namespace Itbid\DataTable\DataType;

use Symfony\Bundle\FrameworkBundle\Translation\Translator;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class AbstractDataType implements DataTypeInterface
{
    /**
     * @var array
     */
    protected $options;

    /**
     * @var ContainerInterface
     */
    protected $container;

    /**
     * @var Translator
     */
    protected $trans;

    public function __construct(ContainerInterface $container, array $options)
    {
        $this->container = $container;
        $this->trans = $container->get('translator');
        $this->options = $options;
    }

    public function transform($data): string
    {
        return (string) $data;
    }

    public function toString($data): string
    {
        return $this->transform($data);
    }

    public static function configureOptions(OptionsResolver $resolver)
    {
        // por defecto, no resolvemos nada
    }
}