<?php

namespace Itbid\DataTable\DataType;

class MatchDataType extends AbstractDataType
{
    const MATCHING_COINCIDENTE = 'coincidente';
    const MATCHING_DISCREPANTE = 'discrepante';

    public function transform($data): string
    {

        if ($data === self::MATCHING_COINCIDENTE){
            return '<span class="ok"><i title="Coincidente" class="icon-large icon-ok-sign text-success"></i></span>';
        }

        if ($data === self::MATCHING_DISCREPANTE){
            return '<span class="warning"><i title="Discrepante" class="icon-large icon-warning-sign text-warning"></i></span>';
        }

        return '--';
    }

    public function toString($data): string
    {
        if ($data === self::MATCHING_COINCIDENTE) {
            return 'str_coincidente';
        }

        if ($data === self::MATCHING_DISCREPANTE) {
            return 'str_discrepante';
        }

        return '--';
    }
}