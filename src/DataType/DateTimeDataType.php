<?php

namespace Itbid\DataTable\DataType;

use DateTime;
use Exception;

class DateTimeDataType extends AbstractDataType
{

    /**
     * @throws Exception
     */
    public function transform($data): string
    {
        if (!$data instanceof DateTime){
            return '--';
        }

        return $data->format('d-m-Y');
    }

}