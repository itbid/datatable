<?php

namespace Itbid\DataTable\DataType;

interface DataTypeInterface
{
    public function transform($data);

    public function toString($data): string;
}


