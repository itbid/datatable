<?php

namespace Itbid\DataTable\DataType;

use Doctrine\Common\Collections\Collection;
use Exception;

class ListReferenceEntityDataType extends AbstractDataType
{

    /**
     * @throws Exception
     */
    public function transform($data): string
    {
        if (!$data instanceof Collection || $data->isEmpty()) {
            return '--';
        }

        $html = '<ul>';

        foreach ($data as $entity) {
            $html .= '<li>'.$this->getReference($entity).'</li>';
        }

        $html .= '</ul>';

        return $html;
    }

    public function toString($data): string
    {
        $referencias = [];
        foreach ($data as $entity) {
            $referencias[] = $this->getReference($entity);
        }

        return implode(', ', $referencias);
    }

    private function getReference($entity)
    {
        if (method_exists($entity, 'getReferencia')) {
            return $entity->getReferencia();
        }

        if (method_exists($entity, 'getTitulo')) {
            return $entity->getTitulo();
        }

        if (method_exists($entity, 'getTitle')) {
            return $entity->getTitle();
        }

        if (method_exists($entity, '__toString')) {
            return $entity->__toString();
        }

        throw new Exception('Entity does not have a __toString() method');
    }

}