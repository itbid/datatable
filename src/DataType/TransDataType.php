<?php

namespace Itbid\DataTable\DataType;

use App\Itbid\AdministracionBundle\Service\TranslatorInterface\Translator;

/**
 * @deprecated (El string dataType ya permite traducir elementos)
 */
class TransDataType extends StringDataType
{

}