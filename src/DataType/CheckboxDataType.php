<?php

namespace Itbid\DataTable\DataType;

use Exception;

class CheckboxDataType extends AbstractDataType
{

    /**
     * @throws Exception
     */
    public function transform($data): string
    {
        if (!is_bool($data)){
            throw new Exception('El valor no es de tipo booleano');
        }

        $class = 'icon-check';

        if ($data === false){
            $class = 'icon-check-empty';
        }

        return '<i class="'.$class.'"></i>';
    }

    public function toString($data): string
    {
       return $data ? 'str_si' : 'str_no';
    }
}