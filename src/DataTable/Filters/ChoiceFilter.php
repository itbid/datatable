<?php

namespace Itbid\DataTable\DataTable\Filters;

use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\QueryBuilder;

class ChoiceFilter
{
    private $field;
    private $label;
    private $options;
    private $choices;
    private $filters;
    public function __construct($field, $filters, $options = [])
    {
        $this->field = $field;
        $this->options = $options;
        $this->filters = $filters;
    }

    public function configureOptions(OptionsResolver $resolver)
    {/*
        $resolver
            ->setDefaults([
                'query_builder' => function (QueryBuilder $qb) {
                    $qb->andWhere('a.' . $this->alias . '=:' . $this->field)
                        ->setParameter($this->field, $this->filters);
                }
            ]);

        $resolver->setRequired('label')
            ->setRequired('choices');*/
    }
}
