<?php

namespace Itbid\DataTable\DataTable\Traits;

use Symfony\Component\OptionsResolver\OptionsResolver;

trait FilterFunctionsTrait
{
    /**
     * @var $filters
     */
    protected $filters;

    public function getFilters()
    {
        return $this->filters;
    }
    public function configureFilters(array $options = [])
    {
        // HACER: Implementar método configureFilters()
    }

    final protected function addFilter($filter, string $type = null, $options = []): self
    {
        $this->filters[$filter] = [
            'filter' => $filter,
            'type' => $type,
            'options' => $this->resolveFilterOptions($filter, $options),
        ];

        return $this;
    }

    public function resolveFilterOptions($filter, $options = [])
    {
        // HACER con OptionsResolver

        return $options;
    }
}