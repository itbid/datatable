<?php

namespace Itbid\DataTable\DataTable\Traits;

use Itbid\DataTable\DataType\StringDataType;
use phpDocumentor\Reflection\Types\Callable_;
use Symfony\Component\OptionsResolver\OptionsResolver;

trait FieldFunctionsTrait
{
    /**
     * @var array
     */
    protected $fields;

    final public function getFields(): array
    {
        return $this->fields;
    }
    public function configureFields(array $options = [])
    {
        // TODO: Implement configureFields() method.
    }
    final protected function addField($field, $title = '', $options = [])
    {
        $this->fields[$field] = [
            'field' => $field,
            'title' => $title,
            'options' => $this->resolveFieldOptions($options),
        ];

        return $this;
    }

    private function resolveFieldOptions(array $options)
    {
        // Opciones por defecto
        $defaultDataType = StringDataType::class;

        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'dataType' => $defaultDataType,
            'transformer' => null
        ]);

        $resolver->setAllowedTypes('transformer', ['null', 'callable']);

        $dataType = $options['dataType'] ? $options['dataType'] : $defaultDataType;

        // Opciones específicas del dataType
        $dataType::configureOptions($resolver);

        return $resolver->resolve($options);
    }
}