<?php

namespace Itbid\DataTable\DataTable\Traits;

use Symfony\Component\OptionsResolver\OptionsResolver;

trait ActionFunctionsTrait
{
    /**
     * @var array
     */
    protected $actions;
    public function configureActions(array $options = [])
    {
        // TODO: Implement configureActions() method.
    }


    final protected function addAction($action, $icon, $options = [])
    {
        $this->actions[$action] = [
            'action' => $action,
            'icon' => $icon,
            'options' => $this->resolveActionOptions($action, $options)
        ];

        return $this;
    }

    private function resolveActionOptions($action, array $options)
    {
        $resolver = new OptionsResolver();
        $resolver->setDefaults([
            'label' => ucfirst($action),
            'url' => '#',
            'route' => '',
            'params' => [],
            'type' => 'list',
            'color' => 'blue',
        ]);

        $resolver->setAllowedTypes('params', ['array']);

        $resolver->setAllowedValues('type', [self::ACTION_TYPE_LIST, self::ACTION_TYPE_PRIMARY]);

        $options = $resolver->resolve($options);

        // Si en vez de una URL hemos pasado una ruta la generamos
        if ('#' === $options['url'] && '' !== $options['route']) {
            $options['url'] = $this->router->generate($options['route'], $options['params']);
        }

        return $options;
    }


    final public function getActions($type = self::ACTION_TYPE_ALL): array
    {
        if (self::ACTION_TYPE_ALL === $type) {
            return $this->actions;
        }

        $actions = [];
        foreach ($this->getActions() as $action) {
            if ($action['options']['type'] === $type) {
                $actions[] = $action;
            }
        }

        return $actions;
    }

    final public function getPrimaryActions(): array
    {
        return $this->getActions(self::ACTION_TYPE_PRIMARY);
    }

    final public function getListActions(): array
    {
        return $this->getActions(self::ACTION_TYPE_LIST);
    }
}