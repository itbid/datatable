<?php

namespace Itbid\DataTable\DataTable;

use Symfony\Component\OptionsResolver\OptionsResolver;

interface DataTableInterface
{
    public function configureFields(array $options = []);

    public function configureFilters(array $options = []);

    public function configureActions(array $options = []);

    public function configureOptions(OptionsResolver $options);

    public function buildQuery(array $options = []): array;

    public function getFields(): array;

    public function getActions(): array;

    public function getListActions(): array;

    public function getPrimaryActions(): array;
}