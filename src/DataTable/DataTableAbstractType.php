<?php

namespace Itbid\DataTable\DataTable;

use Doctrine\ORM\EntityManager;
use Itbid\DataTable\DataTable\Traits\ActionFunctionsTrait;
use Itbid\DataTable\DataTable\Traits\FieldFunctionsTrait;
use Itbid\DataTable\DataTable\Traits\FilterFunctionsTrait;
use Itbid\DataTable\DataType\StringDataType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

abstract class DataTableAbstractType implements DataTableInterface
{
    const ACTION_TYPE_ALL = 'all';
    const ACTION_TYPE_LIST = 'list';
    const ACTION_TYPE_PRIMARY = 'primary';

    const PAGINATION_DEFAULT_LIMIT = 15;
    const PAGINATION_PARAM_NAME_OFFSET = 'offset';
    const PAGINATION_PARAM_NAME_PAGE = 'pag';
    const PAGINATION_PARAM_NAME_TOTAL_PAGES = 'totalPag';
    const PAGINATION_PARAM_NAME_LIMIT = 'limit';

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var ContainerInterface $container
     */
    protected $container;
    /**
     * @var RouterInterface
     */
    protected $router;

    protected $pagination;

    protected $postData;

    /**
     * @var bool
     */
    private $excel;

    public function __construct(EntityManager $em, RouterInterface $router, RequestStack $request, ContainerInterface $container)
    {
        $this->em = $em;
        $this->router = $router;
        $this->fields = [];
        $this->actions = [];
        $this->filters = [];
        $this->pagination = [];
        $this->postData = $request->getCurrentRequest()->request->all();
        $this->excel = $request->getCurrentRequest()->get('render_excel', false);
        $this->container = $container;
    }

    use FieldFunctionsTrait;
    use FilterFunctionsTrait;
    use ActionFunctionsTrait;

    public function configureOptions(OptionsResolver $options)
    {
        $options->setRequired('data_class');
        $options->setDefaults([
            'excel_export' => false
        ]);
    }

    final public function configurePagination(array $options = []): array
    {
        $resolver = new OptionsResolver();
        $postdata = $this->postData;
        unset($postdata['search']); // Deshacernos del elemento 'search' porque no es necesario genera error <UndefinedOptionsException>

        $this->setDefaultOptionsFromPagination($resolver, $this->excel);
        $this->pagination = $resolver->resolve($postdata);
        $this->pagination[self::PAGINATION_PARAM_NAME_OFFSET] = ($this->pagination[self::PAGINATION_PARAM_NAME_PAGE] - 1) * $this->pagination[self::PAGINATION_PARAM_NAME_LIMIT];
        return $this->pagination;
    }

    private function setDefaultOptionsFromPagination(OptionsResolver $resolver, $excel = false)
    {
        // Si tenemos que reenderizar el excel cambiamos el limit
        $limit = $excel ? 999999 : self::PAGINATION_DEFAULT_LIMIT;
        $resolver->setDefaults([
            self::PAGINATION_PARAM_NAME_TOTAL_PAGES => 1,
            self::PAGINATION_PARAM_NAME_PAGE => 1,
            self::PAGINATION_PARAM_NAME_LIMIT => $limit,
            self::PAGINATION_PARAM_NAME_OFFSET => 0
        ]);
    }

    public function getPagination()
    {
        return $this->pagination;
    }

    /**
     * Esta función genera la query por defecto para un listado de una entidad "plana"
     * Será recomendable sobreescribirla con la que se necesite para cada listado
     * @param array $options
     * @return array
     * @throws \Exception
     */
    public function buildQuery(array $options = []): array
    {
        if (!array_key_exists('data_class', $options)) {
            throw new \Exception('data_class options is requiered');
        }

        $repo = $this->em->getRepository($options['data_class']);

        $total = $repo->createQueryBuilder('a')
            ->select('count(a.id)')
            ->getQuery()
            ->getSingleScalarResult();

        $this->pagination[self::PAGINATION_PARAM_NAME_TOTAL_PAGES] = ceil($total / $this->pagination[self::PAGINATION_PARAM_NAME_LIMIT]);
        $result = $repo->findBy([], null, $this->pagination[self::PAGINATION_PARAM_NAME_LIMIT], $this->pagination[self::PAGINATION_PARAM_NAME_OFFSET]);
        return $result;
    }

    public function renderExcel()
    {
        return $this->excel;
    }
}